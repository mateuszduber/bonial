#!/bin/bash
cd /tmp
### Install Docker by script ###
curl -fsSL 'https://get.docker.com' -o get-docker.sh
sudo sh get-docker.sh
# Add admin user to docker group to not need to enter 'sudo'
sudo usermod -aG docker admin
rm get-docker.sh
### Copy HTML file and set correct privileges ###
sudo mkdir /var/www/html -p
sudo mv index.html /var/www/html/
sudo chmod 775 -R /var/www/html
# Run docker NGINX container with mounted /var/www/html for HTML files 
docker run --name bonial -v /var/www/html:/usr/share/nginx/html:ro -d -p 80:80 nginx

# Configure the AWS Provider.
# Please provide your AWS credentials for correct Terraform working/connection
provider "aws" {
  access_key = "xxx"
  secret_key = "xxx"
  # Choice your region. Remember about changing also Availability Zone for correct data.
  region     = "us-east-1"
}

# Default VPC Resources
resource "aws_default_vpc" "default" {
}

### Load Balancer Availability Zones ###
# Availability Zone A
resource "aws_default_subnet" "default_az_a" {
  availability_zone = "us-east-1a"
}

# Availability Zone B
resource "aws_default_subnet" "default_az_b" {
  availability_zone = "us-east-1b"
}

#Pair generated SSH Key for Instance and AWS
resource "aws_key_pair" "aws_key_terraform" {
  key_name = "aws_key"
  public_key = "${file("aws_key.pub")}"
}

# Set rules for default Security Group
resource "aws_default_security_group" "default" {
  vpc_id      = "${aws_default_vpc.default.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    self = true
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Create a web server
resource "aws_instance" "web" {
  # ami = debian-stretch-hvm-x86_64-gp2-2018-10-01-66564 (ami-03006931f694ea7eb)
  ami = "ami-03006931f694ea7eb"
  instance_type = "t2.micro"
  key_name = "aws_key"
  vpc_security_group_ids = ["${aws_default_vpc.default.default_security_group_id}"]
  # Availability zone of created instance
  availability_zone = "us-east-1a"

  tags {
    Name = "bonial"
  }

  ### Copy and Run script for Docker and NGINX configuration
  # Copy script to instance
  provisioner "file" {
    source      = "run-nginx.sh"
    destination = "/tmp/run-nginx.sh"
      
    connection {
      type = "ssh"
      user = "admin"
      private_key = "${file("aws_key")}"
    }
  }
 
  # Copy html file to instance
  provisioner "file" {
    source      = "index.html"
    destination = "/tmp/index.html"
      
    connection {
      type = "ssh"
      user = "admin"
      private_key = "${file("aws_key")}"
    }
  }

  # Run script on the instance
  provisioner "remote-exec" {
    inline = [
      "sudo chmod 700 /tmp/run-nginx.sh",
      "sudo /tmp/run-nginx.sh .",
      "sudo rm /tmp/run-nginx.sh",
    ]
  
    connection {
      type = "ssh"
      user = "admin"
      private_key = "${file("aws_key")}"
    }
  }
}

# Create Elastic Load Balancer
resource "aws_alb" "alb_html" {
	name		=	"alb-bonial"
	internal	=	false
	security_groups	=	["${aws_default_vpc.default.default_security_group_id}"]
	subnets     =   ["${aws_default_subnet.default_az_a.id}", "${aws_default_subnet.default_az_b.id}"]   
}

# Create Elastic Load Balancer - Target Groups
resource "aws_alb_target_group" "alb_front_http" {
	name	= "alb-html-http"
	#vpc_id	= "${var.vpc_id}"
    vpc_id	= "${aws_default_vpc.default.id}"

	port	= "80"
	protocol	= "HTTP"
	health_check {
        path = "/"
        port = "80"
        protocol = "HTTP"
        healthy_threshold = 2
        unhealthy_threshold = 2
        interval = 5
        timeout = 4
        matcher = "200"
    }
}

# Attach Target Groups to Elastic Load Balancer 
resource "aws_alb_target_group_attachment" "alb_backend-01_http" {
  target_group_arn = "${aws_alb_target_group.alb_front_http.arn}"
  target_id        = "${aws_instance.web.id}"
  port             = 80
}

# Add listener to Elastic Load Balancer
resource "aws_alb_listener" "alb_front_http" {

	load_balancer_arn	=	"${aws_alb.alb_html.arn}"
	port			=	"80"
	protocol		=	"HTTP"
	default_action {
		target_group_arn	=	"${aws_alb_target_group.alb_front_http.arn}"
		type			=	"forward"
	}
}
